import 'persian-date';
import _mergeJSXProps from 'babel-helper-vue-jsx-merge-props';

var mouseDownTarget = void 0;

var handleMouseDown = function handleMouseDown(evt) {
  return mouseDownTarget = evt.target;
};

var clickoutside = {
  bind: function bind(el, binding, vnode) {
    el['@clickoutside'] = function (e) {
      var mouseUpTarget = e.target;
      var popupElm = vnode && vnode.context && vnode.context.popupElm;
      if (mouseDownTarget && mouseUpTarget && !el.contains(mouseUpTarget) && !el.contains(mouseDownTarget) && !(popupElm && (popupElm.contains(mouseDownTarget) || popupElm.contains(mouseUpTarget))) && binding.expression && vnode.context[binding.expression]) {
        binding.value();
      }
    };
    document.addEventListener('mousedown', handleMouseDown);
    document.addEventListener('mouseup', el['@clickoutside']);
  },
  unbind: function unbind(el) {
    document.removeEventListener('mousedown', handleMouseDown);
    document.removeEventListener('mouseup', el['@clickoutside']);
  }
};

function isPlainObject(obj) {
  return Object.prototype.toString.call(obj) === '[object Object]';
}

function isDateObejct(value) {
  return value instanceof Date;
}

function isValidDate(date) {
  if (date === null || date === undefined || date === '') {
    return false;
  }
  return !isNaN(new Date(date).getTime());
}

function isValidRangeDate(date) {
  return Array.isArray(date) && date.length === 2 && isValidDate(date[0]) && isValidDate(date[1]) && new Date(date[1]).getTime() >= new Date(date[0]).getTime();
}

function parseTime(time) {
  var values = (time || '').split(':');
  if (values.length >= 2) {
    var hours = parseInt(values[0], 10);
    var minutes = parseInt(values[1], 10);
    return {
      hours: hours,
      minutes: minutes
    };
  }
  return null;
}

function formatTime(time) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '24';
  var a = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'a';

  var hours = time.hours;
  hours = type === '24' ? hours : hours % 12 || 12;
  hours = hours < 10 ? '0' + hours : hours;
  var minutes = time.minutes < 10 ? '0' + time.minutes : time.minutes;
  var result = hours + ':' + minutes;
  if (type === '12') {
    var suffix = time.hours >= 12 ? 'pm' : 'am';
    if (a === 'A') {
      suffix = suffix.toUpperCase();
    }
    result = result + ' ' + suffix;
  }
  return result;
}

function formatDate(date, format, locale) {
  if (!date) {
    return '';
  }
  try {
    return Date.formatJalaliDate(date, format, locale);
  } catch (e) {
    return '';
  }
}

function parseDate(value, format, locale) {
  try {
    return Date.parseJalaliDate(value, format, locale);
  } catch (e) {
    return null;
  }
}

function throttle(action, delay) {
  var lastRun = 0;
  var timeout = null;
  return function () {
    var _this = this;

    if (timeout) {
      return;
    }
    var args = arguments;
    var elapsed = Date.now() - lastRun;
    var callBack = function callBack() {
      lastRun = Date.now();
      timeout = null;
      action.apply(_this, args);
    };
    if (elapsed >= delay) {
      callBack();
    } else {
      timeout = setTimeout(callBack, delay);
    }
  };
}

var transformDate = {
  date: {
    value2date: function value2date(value) {
      return isValidDate(value) ? new Date(value) : null;
    },
    date2value: function date2value(date) {
      return date;
    }
  },
  timestamp: {
    value2date: function value2date(value) {
      return isValidDate(value) ? new Date(value) : null;
    },
    date2value: function date2value(date) {
      return date && new Date(date).getTime();
    }
  }
};

var Languages = {
  'en': {
    'days': ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    'months': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    'pickers': ['next 7 days', 'next 30 days', 'previous 7 days', 'previous 30 days'],
    'placeholder': {
      'date': 'Select Date',
      'dateRange': 'Select Date Range'
    }
  },
  'fa': {
    'days': ['ی', 'د', 'س', 'چ', 'پ', 'ج', 'ش'],
    'months': ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
    'pickers': ['7 روز بعد', '30 روز بعد', '7 روز قبل', '30 روز قبل'],
    'placeholder': {
      'date': 'انتخاب زمان',
      'dateRange': 'انتخاب محدوده زمانی'
    }
  }
};

var defaultLang = Languages.fa;

var locale = {
  methods: {
    t: function t(path) {
      var component = this;
      var name = component.$options.name;
      while (component && (!name || name !== 'DatePicker')) {
        component = component.$parent;
        if (component) {
          name = component.$options.name;
        }
      }
      var lang = component && component.language || defaultLang;
      var arr = path.split('.');
      var current = lang;
      var value = void 0;
      for (var i = 0, len = arr.length; i < len; i++) {
        var prop = arr[i];
        value = current[prop];
        if (i === len - 1) {
          return value;
        }
        if (!value) {
          return '';
        }
        current = value;
      }
      return '';
    }
  }
};

var emitter = {
  methods: {
    dispatch: function dispatch(componentName, eventName, params) {
      var parent = this.$parent || this.$root;
      var name = parent.$options.name;

      while (parent && (!name || name !== componentName)) {
        parent = parent.$parent;

        if (parent) {
          name = parent.$options.name;
        }
      }
      if (name && name === componentName) {
        parent = parent || this;
        parent.$emit.apply(parent, [eventName].concat(params));
      }
    }
  }
};

function scrollIntoView(container, selected) {
  if (!selected) {
    container.scrollTop = 0;
    return;
  }

  var offsetParents = [];
  var pointer = selected.offsetParent;
  while (pointer && container !== pointer && container.contains(pointer)) {
    offsetParents.push(pointer);
    pointer = pointer.offsetParent;
  }
  var top = selected.offsetTop + offsetParents.reduce(function (prev, curr) {
    return prev + curr.offsetTop;
  }, 0);
  var bottom = top + selected.offsetHeight;
  var viewRectTop = container.scrollTop;
  var viewRectBottom = viewRectTop + container.clientHeight;

  if (top < viewRectTop) {
    container.scrollTop = top;
  } else if (bottom > viewRectBottom) {
    container.scrollTop = bottom - container.clientHeight;
  }
}

var PanelDate = {
  name: 'panelDate',
  mixins: [locale],
  props: {
    value: null,
    startAt: null,
    endAt: null,
    dateFormat: {
      type: String,
      default: 'YYYY-MM-DD'
    },
    calendarMonth: {
      default: new Date().getJalaliMonth()
    },
    calendarYear: {
      default: new Date().getJalaliFullYear()
    },
    firstDayOfWeek: {
      default: 7,
      type: Number,
      validator: function validator(val) {
        return val >= 1 && val <= 7;
      }
    },
    disabledDate: {
      type: Function,
      default: function _default() {
        return false;
      }
    }
  },
  methods: {
    selectDate: function selectDate(_ref) {
      var year = _ref.year,
          month = _ref.month,
          day = _ref.day;

      var date = new Date(new Date(null, null).setJalaliFullYear(year, month, day));
      if (this.disabledDate(date)) {
        return;
      }
      this.$emit('select', date);
    },
    getDays: function getDays(firstDayOfWeek) {
      var days = this.t('days');
      var firstday = parseInt(firstDayOfWeek, 10);
      return days.concat(days).slice(firstday, firstday + 7);
    },
    getDates: function getDates(year, month, firstDayOfWeek) {
      var arr = [];
      var time = new Date(new Date(null, null).setJalaliFullYear(year, month, 1));

      time.setJalaliDate(0); // 把时间切换到上个月最后一天
      var lastMonthLength = (time.getDay() + 7 - firstDayOfWeek) % 7 + 1; // time.getDay() 0是星期天, 1是星期一 ...
      var lastMonthfirst = time.getJalaliDate() - (lastMonthLength - 1);
      for (var i = 0; i < lastMonthLength; i++) {
        arr.push({ year: year, month: month - 1, day: lastMonthfirst + i });
      }

      time.setJalaliMonth(time.getJalaliMonth() + 2, 0); // 切换到这个月最后一天
      var curMonthLength = time.getJalaliDate();
      for (var _i = 0; _i < curMonthLength; _i++) {
        arr.push({ year: year, month: month, day: 1 + _i });
      }

      time.setJalaliMonth(time.getJalaliMonth() + 1, 1); // 切换到下个月第一天
      var nextMonthLength = 42 - (lastMonthLength + curMonthLength);
      for (var _i2 = 0; _i2 < nextMonthLength; _i2++) {
        arr.push({ year: year, month: month + 1, day: 1 + _i2 });
      }

      return arr;
    },
    getCellClasses: function getCellClasses(date, _ref2) {
      var month = _ref2.month;

      var classes = [];
      var cellTime = new Date(date).setHours(0, 0, 0, 0);
      var today = new Date().setHours(0, 0, 0, 0);
      var curTime = this.value && new Date(this.value).setHours(0, 0, 0, 0);
      var startTime = this.startAt && new Date(this.startAt).setHours(0, 0, 0, 0);
      var endTime = this.endAt && new Date(this.endAt).setHours(0, 0, 0, 0);

      if (month < this.calendarMonth) {
        classes.push('last-month');
      } else if (month > this.calendarMonth) {
        classes.push('next-month');
      } else {
        classes.push('cur-month');
      }

      if (cellTime === today) {
        classes.push('today');
      }

      if (this.disabledDate(cellTime)) {
        classes.push('disabled');
      }

      if (curTime) {
        if (cellTime === curTime) {
          classes.push('actived');
        } else if (startTime && cellTime <= curTime) {
          classes.push('inrange');
        } else if (endTime && cellTime >= curTime) {
          classes.push('inrange');
        }
      }
      return classes;
    },
    getCellTitle: function getCellTitle(date) {
      return formatDate(date, this.dateFormat);
    }
  },
  render: function render(h) {
    var _this = this;

    var ths = this.getDays(this.firstDayOfWeek).map(function (day) {
      return h('th', [day]);
    });

    var dates = this.getDates(this.calendarYear, this.calendarMonth, this.firstDayOfWeek);
    var tbody = Array.apply(null, { length: 6 }).map(function (week, i) {
      var tds = dates.slice(7 * i, 7 * i + 7).map(function (dateObj) {
        var date = new Date(new Date(null, null).setJalaliFullYear(dateObj.year, dateObj.month, dateObj.day));
        var attrs = {
          class: _this.getCellClasses(date, dateObj)
        };
        return h(
          'td',
          _mergeJSXProps([{
            'class': 'cell'
          }, attrs, {
            attrs: {
              'data-year': date.year,
              'data-month': date.month,
              title: _this.getCellTitle(date)
            },
            on: {
              'click': _this.selectDate.bind(_this, dateObj)
            }
          }]),
          [dateObj.day]
        );
      });
      return h('tr', [tds]);
    });

    return h(
      'table',
      { 'class': 'mx-panel mx-panel-date' },
      [h('thead', [h('tr', [ths])]), h('tbody', [tbody])]
    );
  }
};

var PanelYear = {
  name: 'panelYear',
  props: {
    value: null,
    firstYear: Number,
    disabledYear: Function
  },
  methods: {
    isDisabled: function isDisabled(year) {
      if (typeof this.disabledYear === 'function' && this.disabledYear(year)) {
        return true;
      }
      return false;
    },
    selectYear: function selectYear(year) {
      if (this.isDisabled(year)) {
        return;
      }
      this.$emit('select', year);
    }
  },
  render: function render(h) {
    var _this = this;

    // 当前年代
    var firstYear = Math.floor(this.firstYear / 10) * 10;
    var currentYear = this.value && new Date(this.value).getJalaliFullYear();
    var years = Array.apply(null, { length: 10 }).map(function (_, i) {
      var year = firstYear + i;
      return h(
        'span',
        {
          'class': {
            'cell': true,
            'actived': currentYear === year,
            'disabled': _this.isDisabled(year)
          },
          on: {
            'click': _this.selectYear.bind(_this, year)
          }
        },
        [year]
      );
    });
    return h(
      'div',
      { 'class': 'mx-panel mx-panel-year' },
      [years]
    );
  }
};

var PanelMonth = {
  name: 'panelMonth',
  mixins: [locale],
  props: {
    value: null,
    calendarYear: {
      default: new Date().getJalaliFullYear()
    },
    disabledMonth: Function
  },
  methods: {
    isDisabled: function isDisabled(month) {
      if (typeof this.disabledMonth === 'function' && this.disabledMonth(month)) {
        return true;
      }
      return false;
    },
    selectMonth: function selectMonth(month) {
      if (this.isDisabled(month)) {
        return;
      }
      this.$emit('select', month);
    }
  },
  render: function render(h) {
    var _this = this;

    var months = this.t('months');
    var currentYear = this.value && new Date(this.value).getJalaliFullYear();
    var currentMonth = this.value && new Date(this.value).getJalaliMonth();
    months = months.map(function (v, i) {
      return h(
        'span',
        {
          'class': {
            'cell': true,
            'actived': currentYear === _this.calendarYear && currentMonth === i,
            'disabled': _this.isDisabled(i)
          },
          on: {
            'click': _this.selectMonth.bind(_this, i)
          }
        },
        [v]
      );
    });
    return h(
      'div',
      { 'class': 'mx-panel mx-panel-month' },
      [months]
    );
  }
};

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var PanelTime = {
  name: 'panelTime',
  props: {
    timePickerOptions: {
      type: [Object, Function],
      default: function _default() {
        return null;
      }
    },
    timeSelectOptions: {
      type: Object,
      default: function _default() {
        return null;
      }
    },
    minuteStep: {
      type: Number,
      default: 0,
      validator: function validator(val) {
        return val >= 0 && val <= 60;
      }
    },
    value: null,
    timeType: {
      type: Array,
      default: function _default() {
        return ['24', 'a'];
      }
    },
    disabledTime: Function
  },
  computed: {
    currentHours: function currentHours() {
      return this.value ? new Date(this.value).getHours() : 0;
    },
    currentMinutes: function currentMinutes() {
      return this.value ? new Date(this.value).getMinutes() : 0;
    },
    currentSeconds: function currentSeconds() {
      return this.value ? new Date(this.value).getSeconds() : 0;
    }
  },
  methods: {
    stringifyText: function stringifyText(value) {
      return ('00' + value).slice(String(value).length);
    },
    selectTime: function selectTime(time) {
      if (typeof this.disabledTime === 'function' && this.disabledTime(time)) {
        return;
      }
      this.$emit('select', new Date(time));
    },
    pickTime: function pickTime(time) {
      if (typeof this.disabledTime === 'function' && this.disabledTime(time)) {
        return;
      }
      this.$emit('pick', new Date(time));
    },
    getTimePickerOptions: function getTimePickerOptions() {
      var result = [];
      var options = this.timePickerOptions;
      if (!options) {
        return [];
      }
      if (typeof options === 'function') {
        return options() || [];
      }
      var start = parseTime(options.start);
      var end = parseTime(options.end);
      var step = parseTime(options.step);
      if (start && end && step) {
        var startMinutes = start.minutes + start.hours * 60;
        var endMinutes = end.minutes + end.hours * 60;
        var stepMinutes = step.minutes + step.hours * 60;
        var len = Math.floor((endMinutes - startMinutes) / stepMinutes);
        for (var i = 0; i <= len; i++) {
          var timeMinutes = startMinutes + i * stepMinutes;
          var hours = Math.floor(timeMinutes / 60);
          var minutes = timeMinutes % 60;
          var value = {
            hours: hours,
            minutes: minutes
          };
          result.push({
            value: value,
            label: formatTime.apply(undefined, [value].concat(_toConsumableArray(this.timeType)))
          });
        }
      }
      return result;
    }
  },
  render: function render(h) {
    var _this = this;

    var date = this.value ? new Date(this.value) : new Date().setHours(0, 0, 0, 0);
    var disabledTime = typeof this.disabledTime === 'function' && this.disabledTime;

    var pickers = this.getTimePickerOptions();
    if (Array.isArray(pickers) && pickers.length) {
      pickers = pickers.map(function (picker) {
        var pickHours = picker.value.hours;
        var pickMinutes = picker.value.minutes;
        var time = new Date(date).setHours(pickHours, pickMinutes, 0);
        return h(
          'li',
          {
            'class': {
              'mx-time-picker-item': true,
              'cell': true,
              'actived': pickHours === _this.currentHours && pickMinutes === _this.currentMinutes,
              'disabled': disabledTime && disabledTime(time)
            },
            on: {
              'click': _this.pickTime.bind(_this, time)
            }
          },
          [picker.label]
        );
      });
      return h(
        'div',
        { 'class': 'mx-panel mx-panel-time' },
        [h(
          'ul',
          { 'class': 'mx-time-list' },
          [pickers]
        )]
      );
    }

    var minuteStep = this.minuteStep || 1;
    var minuteLength = parseInt(60 / minuteStep);
    var hours = Array.apply(null, { length: 24 }).map(function (_, i) {
      return i;
    });
    var minutes = Array.apply(null, { length: minuteLength }).map(function (_, i) {
      return i * minuteStep;
    });
    var seconds = this.minuteStep === 0 ? Array.apply(null, { length: 60 }).map(function (_, i) {
      return i;
    }) : [];
    var columns = { hours: hours, minutes: minutes, seconds: seconds };

    if (this.timeSelectOptions && _typeof(this.timeSelectOptions) === 'object') {
      columns = _extends({}, columns, this.timeSelectOptions);
    }

    var hoursColumn = columns.hours.map(function (v) {
      var time = new Date(date).setHours(v);
      return h(
        'li',
        {
          'class': {
            'cell': true,
            'actived': v === _this.currentHours,
            'disabled': disabledTime && disabledTime(time)
          },
          on: {
            'click': _this.selectTime.bind(_this, time)
          }
        },
        [_this.stringifyText(v)]
      );
    });

    var minutesColumn = columns.minutes.map(function (v) {
      var time = new Date(date).setMinutes(v);
      return h(
        'li',
        {
          'class': {
            'cell': true,
            'actived': v === _this.currentMinutes,
            'disabled': disabledTime && disabledTime(time)
          },
          on: {
            'click': _this.selectTime.bind(_this, time)
          }
        },
        [_this.stringifyText(v)]
      );
    });

    var secondsColumn = columns.seconds.map(function (v) {
      var time = new Date(date).setSeconds(v);
      return h(
        'li',
        {
          'class': {
            'cell': true,
            'actived': v === _this.currentSeconds,
            'disabled': disabledTime && disabledTime(time)
          },
          on: {
            'click': _this.selectTime.bind(_this, time)
          }
        },
        [_this.stringifyText(v)]
      );
    });

    var times = [hoursColumn, minutesColumn, secondsColumn].filter(function (v) {
      return v.length > 0;
    });

    times = times.map(function (list) {
      return h(
        'ul',
        { 'class': 'mx-time-list', style: { width: 100 / times.length + '%' } },
        [list]
      );
    });

    return h(
      'div',
      { 'class': 'mx-panel mx-panel-time' },
      [times]
    );
  }
};

//
var script = {
  name: 'CalendarPanel',
  components: { PanelDate: PanelDate, PanelYear: PanelYear, PanelMonth: PanelMonth, PanelTime: PanelTime },
  mixins: [locale, emitter],
  props: {
    value: {
      default: null,
      validator: function validator(val) {
        return val === null || isValidDate(val);
      }
    },
    startAt: null,
    endAt: null,
    visible: {
      type: Boolean,
      default: false
    },
    type: {
      type: String,
      default: 'date' // ['date', 'datetime'] zendy added 'month', 'year', mxie added "time"
    },
    dateFormat: {
      type: String,
      default: 'YYYY-MM-DD'
    },
    index: Number,
    // below user set
    defaultValue: {
      validator: function validator(val) {
        return isValidDate(val);
      }
    },
    firstDayOfWeek: {
      default: 6,
      type: Number,
      validator: function validator(val) {
        return val >= 1 && val <= 7;
      }
    },
    notBefore: {
      default: null,
      validator: function validator(val) {
        return !val || isValidDate(val);
      }
    },
    notAfter: {
      default: null,
      validator: function validator(val) {
        return !val || isValidDate(val);
      }
    },
    disabledDays: {
      type: [Array, Function],
      default: function _default() {
        return [];
      }
    },
    minuteStep: {
      type: Number,
      default: 0,
      validator: function validator(val) {
        return val >= 0 && val <= 60;
      }
    },
    timeSelectOptions: {
      type: Object,
      default: function _default() {
        return null;
      }
    },
    timePickerOptions: {
      type: [Object, Function],
      default: function _default() {
        return null;
      }
    }
  },
  data: function data() {
    var now = this.getNow(this.value);
    var calendarYear = now.getJalaliFullYear();
    var calendarMonth = now.getJalaliMonth();
    var firstYear = Math.floor(calendarYear / 10) * 10;
    return {
      panel: 'NONE',
      dates: [],
      calendarMonth: calendarMonth,
      calendarYear: calendarYear,
      firstYear: firstYear
    };
  },

  computed: {
    now: {
      get: function get() {
        return new Date(new Date(null, null).setJalaliFullYear(this.calendarYear, this.calendarMonth, 1));
      },
      set: function set(val) {
        var now = new Date(val);
        this.calendarYear = now.getJalaliFullYear();
        this.calendarMonth = now.getJalaliMonth();
      }
    },
    timeType: function timeType() {
      var h = /h+/.test(this.$parent.format) ? '12' : '24';
      var a = /A/.test(this.$parent.format) ? 'A' : 'a';
      return [h, a];
    },
    timeHeader: function timeHeader() {
      if (this.type === 'time') {
        return this.$parent.format;
      }
      return this.value && formatDate(this.value, this.dateFormat, this.$parent.lang);
    },
    yearHeader: function yearHeader() {
      return this.firstYear + ' ~ ' + (this.firstYear + 10);
    },
    months: function months() {
      return this.t('months');
    },
    notBeforeTime: function notBeforeTime() {
      return this.getCriticalTime(this.notBefore);
    },
    notAfterTime: function notAfterTime() {
      return this.getCriticalTime(this.notAfter);
    }
  },
  watch: {
    value: {
      immediate: true,
      handler: 'updateNow'
    },
    visible: {
      immediate: true,
      handler: 'init'
    },
    panel: {
      handler: 'handelPanelChange'
    }
  },
  methods: {
    handelPanelChange: function handelPanelChange(panel, oldPanel) {
      var _this = this;

      this.dispatch('DatePicker', 'panel-change', [panel, oldPanel]);
      if (panel === 'YEAR') {
        this.firstYear = Math.floor(this.calendarYear / 10) * 10;
      } else if (panel === 'TIME') {
        this.$nextTick(function () {
          var list = _this.$el.querySelectorAll('.mx-panel-time .mx-time-list');
          for (var i = 0, len = list.length; i < len; i++) {
            var el = list[i];
            scrollIntoView(el, el.querySelector('.actived'));
          }
        });
      }
    },
    init: function init(val) {
      if (val) {
        var type = this.type;
        if (type === 'month') {
          this.showPanelMonth();
        } else if (type === 'year') {
          this.showPanelYear();
        } else if (type === 'time') {
          this.showPanelTime();
        } else {
          this.showPanelDate();
        }
      } else {
        this.showPanelNone();
        this.updateNow(this.value);
      }
    },
    getNow: function getNow(value) {
      return value ? new Date(value) : this.defaultValue && isValidDate(this.defaultValue) ? new Date(this.defaultValue) : new Date();
    },

    // 根据value更新日历
    updateNow: function updateNow(value) {
      var oldNow = this.now;
      this.now = this.getNow(value);
      if (this.visible && this.now !== oldNow) {
        this.dispatch('DatePicker', 'calendar-change', [new Date(this.now), new Date(oldNow)]);
      }
    },
    getCriticalTime: function getCriticalTime(value) {
      if (!value) {
        return null;
      }
      var date = new Date(value);
      if (this.type === 'year') {
        return new Date(date.getFullYear(), 0).getTime();
      } else if (this.type === 'month') {
        return new Date(date.getFullYear(), date.getMonth()).getTime();
      } else if (this.type === 'date') {
        return date.setHours(0, 0, 0, 0);
      }
      return date.getTime();
    },
    inBefore: function inBefore(time, startAt) {
      if (startAt === undefined) {
        startAt = this.startAt;
      }
      return this.notBeforeTime && time < this.notBeforeTime || startAt && time < this.getCriticalTime(startAt);
    },
    inAfter: function inAfter(time, endAt) {
      if (endAt === undefined) {
        endAt = this.endAt;
      }
      return this.notAfterTime && time > this.notAfterTime || endAt && time > this.getCriticalTime(endAt);
    },
    inDisabledDays: function inDisabledDays(time) {
      var _this2 = this;

      if (Array.isArray(this.disabledDays)) {
        return this.disabledDays.some(function (v) {
          return _this2.getCriticalTime(v) === time;
        });
      } else if (typeof this.disabledDays === 'function') {
        return this.disabledDays(new Date(time));
      }
      return false;
    },
    isDisabledYear: function isDisabledYear(year) {
      var time = new Date(null, null).setJalaliFullYear(year, 0, 1);
      var maxTime = new Date(null, null).setJalaliFullYear(year + 1, 0, 1) - 1;
      return this.inBefore(maxTime) || this.inAfter(time) || this.type === 'year' && this.inDisabledDays(time);
    },
    isDisabledMonth: function isDisabledMonth(month) {
      var time = new Date(null, null).setJalaliFullYear(this.calendarYear, month, 1);
      var maxTime = new Date(null, null).setJalaliFullYear(this.calendarYear, month + 1, 1) - 1;
      return this.inBefore(maxTime) || this.inAfter(time) || this.type === 'month' && this.inDisabledDays(time);
    },
    isDisabledDate: function isDisabledDate(date) {
      var time = new Date(date).getTime();
      var maxTime = new Date(date).setHours(23, 59, 59, 999);
      return this.inBefore(maxTime) || this.inAfter(time) || this.inDisabledDays(time);
    },
    isDisabledTime: function isDisabledTime(date, startAt, endAt) {
      var time = new Date(date).getTime();
      return this.inBefore(time, startAt) || this.inAfter(time, endAt) || this.inDisabledDays(time);
    },
    selectDate: function selectDate(date) {
      if (this.type === 'datetime') {
        var time = new Date(date);
        if (isDateObejct(this.value)) {
          time.setHours(this.value.getHours(), this.value.getMinutes(), this.value.getSeconds());
        }
        if (this.isDisabledTime(time)) {
          time.setHours(0, 0, 0, 0);
          if (this.notBefore && time.getTime() < new Date(this.notBefore).getTime()) {
            time = new Date(this.notBefore);
          }
          if (this.startAt && time.getTime() < new Date(this.startAt).getTime()) {
            time = new Date(this.startAt);
          }
        }
        this.selectTime(time);
        this.showPanelTime();
        return;
      }
      this.$emit('select-date', date);
    },
    selectYear: function selectYear(year) {
      this.changeCalendarYear(year);
      if (this.type.toLowerCase() === 'year') {
        return this.selectDate(new Date(this.now));
      }
      this.dispatch('DatePicker', 'select-year', [year, this.index]);
      this.showPanelMonth();
    },
    selectMonth: function selectMonth(month) {
      this.changeCalendarMonth(month);
      if (this.type.toLowerCase() === 'month') {
        return this.selectDate(new Date(this.now));
      }
      this.dispatch('DatePicker', 'select-month', [month, this.index]);
      this.showPanelDate();
    },
    selectTime: function selectTime(time) {
      this.$emit('select-time', time, false);
    },
    pickTime: function pickTime(time) {
      this.$emit('select-time', time, true);
    },
    changeCalendarYear: function changeCalendarYear(year) {
      this.updateNow(new Date(new Date(null, null).setJalaliFullYear(year, this.calendarMonth, 1)));
    },
    changeCalendarMonth: function changeCalendarMonth(month) {
      if (month === -1) {
        this.calendarYear = this.calendarYear - 1;
        month = 11;
      }
      this.updateNow(new Date(new Date(null, null).setJalaliFullYear(this.calendarYear, month, 1)));
    },
    getSibling: function getSibling() {
      var _this3 = this;

      var calendars = this.$parent.$children.filter(function (v) {
        return v.$options.name === _this3.$options.name;
      });
      var index = calendars.indexOf(this);
      var sibling = calendars[index ^ 1];
      return sibling;
    },
    handleIconMonth: function handleIconMonth(flag) {
      var month = this.calendarMonth;
      this.changeCalendarMonth(month + flag);
      this.$parent.$emit('change-calendar-month', {
        month: month,
        flag: flag,
        vm: this,
        sibling: this.getSibling()
      });
    },
    handleIconYear: function handleIconYear(flag) {
      if (this.panel === 'YEAR') {
        this.changePanelYears(flag);
      } else {
        var year = this.calendarYear;
        this.changeCalendarYear(year + flag);
        this.$parent.$emit('change-calendar-year', {
          year: year,
          flag: flag,
          vm: this,
          sibling: this.getSibling()
        });
      }
    },
    handleBtnYear: function handleBtnYear() {
      this.showPanelYear();
    },
    handleBtnMonth: function handleBtnMonth() {
      this.showPanelMonth();
    },
    handleTimeHeader: function handleTimeHeader() {
      if (this.type === 'time') {
        return;
      }
      this.showPanelDate();
    },
    changePanelYears: function changePanelYears(flag) {
      this.firstYear = this.firstYear + flag * 10;
    },
    showPanelNone: function showPanelNone() {
      this.panel = 'NONE';
    },
    showPanelTime: function showPanelTime() {
      this.panel = 'TIME';
    },
    showPanelDate: function showPanelDate() {
      this.panel = 'DATE';
    },
    showPanelYear: function showPanelYear() {
      this.panel = 'YEAR';
    },
    showPanelMonth: function showPanelMonth() {
      this.panel = 'MONTH';
    }
  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function __vue_render__() {
  var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;return _c('div', { staticClass: "mx-calendar", class: 'mx-calendar-panel-' + _vm.panel.toLowerCase() }, [_c('div', { staticClass: "mx-calendar-header" }, [_c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel !== 'TIME', expression: "panel !== 'TIME'" }], staticClass: "mx-icon-last-year", on: { "click": function click($event) {
        return _vm.handleIconYear(-1);
      } } }, [_vm._v("«")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], staticClass: "mx-icon-last-month", on: { "click": function click($event) {
        return _vm.handleIconMonth(-1);
      } } }, [_vm._v("‹")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel !== 'TIME', expression: "panel !== 'TIME'" }], staticClass: "mx-icon-next-year", on: { "click": function click($event) {
        return _vm.handleIconYear(1);
      } } }, [_vm._v("»")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], staticClass: "mx-icon-next-month", on: { "click": function click($event) {
        return _vm.handleIconMonth(1);
      } } }, [_vm._v("›")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], staticClass: "mx-current-month", on: { "click": _vm.handleBtnMonth } }, [_vm._v(_vm._s(_vm.months[_vm.calendarMonth]))]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE' || _vm.panel === 'MONTH', expression: "panel === 'DATE' || panel === 'MONTH'" }], staticClass: "mx-current-year", on: { "click": _vm.handleBtnYear } }, [_vm._v(_vm._s(_vm.calendarYear))]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'YEAR', expression: "panel === 'YEAR'" }], staticClass: "mx-current-year" }, [_vm._v(_vm._s(_vm.yearHeader))]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'TIME', expression: "panel === 'TIME'" }], staticClass: "mx-time-header", on: { "click": _vm.handleTimeHeader } }, [_vm._v(_vm._s(_vm.timeHeader))])]), _vm._v(" "), _c('div', { staticClass: "mx-calendar-content" }, [_c('panel-date', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], attrs: { "value": _vm.value, "date-format": _vm.dateFormat, "calendar-month": _vm.calendarMonth, "calendar-year": _vm.calendarYear, "start-at": _vm.startAt, "end-at": _vm.endAt, "first-day-of-week": _vm.firstDayOfWeek, "disabled-date": _vm.isDisabledDate }, on: { "select": _vm.selectDate } }), _vm._v(" "), _c('panel-year', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'YEAR', expression: "panel === 'YEAR'" }], attrs: { "value": _vm.value, "disabled-year": _vm.isDisabledYear, "first-year": _vm.firstYear }, on: { "select": _vm.selectYear } }), _vm._v(" "), _c('panel-month', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'MONTH', expression: "panel === 'MONTH'" }], attrs: { "value": _vm.value, "disabled-month": _vm.isDisabledMonth, "calendar-year": _vm.calendarYear }, on: { "select": _vm.selectMonth } }), _vm._v(" "), _c('panel-time', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'TIME', expression: "panel === 'TIME'" }], attrs: { "minute-step": _vm.minuteStep, "time-picker-options": _vm.timePickerOptions, "time-select-options": _vm.timeSelectOptions, "value": _vm.value, "disabled-time": _vm.isDisabledTime, "time-type": _vm.timeType }, on: { "select": _vm.selectTime, "pick": _vm.pickTime } })], 1)]);
};
var __vue_staticRenderFns__ = [];

/* style */
var __vue_inject_styles__ = undefined;
/* scoped */
var __vue_scope_id__ = undefined;
/* module identifier */
var __vue_module_identifier__ = undefined;
/* functional template */
var __vue_is_functional_template__ = false;
/* style inject */

/* style inject SSR */

var CalendarPanel = normalizeComponent_1({ render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ }, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, undefined, undefined);

var _extends$1 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var script$1 = {
  name: 'DatePicker',
  components: { CalendarPanel: CalendarPanel },
  mixins: [locale],
  directives: {
    clickoutside: clickoutside
  },
  props: {
    value: null,
    valueType: {
      default: 'date',
      validator: function validator(value) {
        return ['timestamp', 'format', 'date'].indexOf(value) !== -1 || isPlainObject(value);
      }
    },
    placeholder: {
      type: String,
      default: null
    },
    lang: {
      type: [String, Object],
      default: 'fa'
    },
    format: {
      type: [String, Object],
      default: 'YYYY-MM-DD'
    },
    dateFormat: {
      type: String // format the time header and date tooltip
    },
    type: {
      type: String,
      default: 'date' // ['date', 'datetime'] zendy added 'month', 'year', mxie added "time"
    },
    range: {
      type: Boolean,
      default: false
    },
    rangeSeparator: {
      type: String,
      default: '~'
    },
    width: {
      type: [String, Number],
      default: null
    },
    confirmText: {
      type: String,
      default: 'OK'
    },
    confirm: {
      type: Boolean,
      default: false
    },
    editable: {
      type: Boolean,
      default: true
    },
    disabled: {
      type: Boolean,
      default: false
    },
    clearable: {
      type: Boolean,
      default: true
    },
    shortcuts: {
      type: [Boolean, Array],
      default: true
    },
    inputName: {
      type: String,
      default: 'date'
    },
    inputClass: {
      type: [String, Array],
      default: 'mx-input'
    },
    inputAttr: Object,
    appendToBody: {
      type: Boolean,
      default: false
    },
    popupStyle: {
      type: Object
    }
  },
  data: function data() {
    return {
      currentValue: this.range ? [null, null] : null,
      userInput: null,
      popupVisible: false,
      position: {}
    };
  },

  watch: {
    value: {
      immediate: true,
      handler: 'handleValueChange'
    },
    popupVisible: function popupVisible(val) {
      if (val) {
        this.initCalendar();
      } else {
        this.userInput = null;
        this.blur();
      }
    }
  },
  computed: {
    transform: function transform() {
      var type = this.valueType;
      if (isPlainObject(type)) {
        return _extends$1({}, transformDate.date, type);
      }
      if (type === 'format') {
        return {
          value2date: this.parse.bind(this),
          date2value: this.stringify.bind(this)
        };
      }
      return transformDate[type] || transformDate.date;
    },
    language: function language() {
      if (isPlainObject(this.lang)) {
        return _extends$1({}, Languages.en, this.lang);
      }
      return Languages[this.lang] || Languages.en;
    },
    innerPlaceholder: function innerPlaceholder() {
      if (typeof this.placeholder === 'string') {
        return this.placeholder;
      }
      return this.range ? this.t('placeholder.dateRange') : this.t('placeholder.date');
    },
    text: function text() {
      if (this.userInput !== null) {
        return this.userInput;
      }
      var value2date = this.transform.value2date;

      if (!this.range) {
        return this.isValidValue(this.value) ? this.stringify(value2date(this.value)) : '';
      }
      return this.isValidRangeValue(this.value) ? this.stringify(value2date(this.value[0])) + ' ' + this.rangeSeparator + ' ' + this.stringify(value2date(this.value[1])) : '';
    },
    computedWidth: function computedWidth() {
      if (typeof this.width === 'number' || typeof this.width === 'string' && /^\d+$/.test(this.width)) {
        return this.width + 'px';
      }
      return this.width;
    },
    showClearIcon: function showClearIcon() {
      return !this.disabled && this.clearable && (this.range ? this.isValidRangeValue(this.value) : this.isValidValue(this.value));
    },
    innerType: function innerType() {
      return String(this.type).toLowerCase();
    },
    innerShortcuts: function innerShortcuts() {
      if (Array.isArray(this.shortcuts)) {
        return this.shortcuts;
      }
      if (this.shortcuts === false) {
        return [];
      }
      var pickers = this.t('pickers');
      var arr = [{
        text: pickers[0],
        onClick: function onClick(self) {
          self.currentValue = [new Date(), new Date(Date.now() + 3600 * 1000 * 24 * 7)];
          self.updateDate(true);
        }
      }, {
        text: pickers[1],
        onClick: function onClick(self) {
          self.currentValue = [new Date(), new Date(Date.now() + 3600 * 1000 * 24 * 30)];
          self.updateDate(true);
        }
      }, {
        text: pickers[2],
        onClick: function onClick(self) {
          self.currentValue = [new Date(Date.now() - 3600 * 1000 * 24 * 7), new Date()];
          self.updateDate(true);
        }
      }, {
        text: pickers[3],
        onClick: function onClick(self) {
          self.currentValue = [new Date(Date.now() - 3600 * 1000 * 24 * 30), new Date()];
          self.updateDate(true);
        }
      }];
      return arr;
    },
    innerDateFormat: function innerDateFormat() {
      if (this.dateFormat) {
        return this.dateFormat;
      }
      if (typeof this.format !== 'string') {
        return 'YYYY-MM-DD';
      }
      if (this.innerType === 'date') {
        return this.format;
      }
      return this.format.replace(/[Hh]+.*[msSaAZ]|\[.*?\]/g, '').trim() || 'YYYY-MM-DD';
    },
    innerPopupStyle: function innerPopupStyle() {
      return _extends$1({}, this.position, this.popupStyle);
    }
  },
  mounted: function mounted() {
    var _this = this;

    if (this.appendToBody) {
      this.popupElm = this.$refs.calendar;
      document.body.appendChild(this.popupElm);
    }
    this._displayPopup = throttle(function () {
      if (_this.popupVisible) {
        _this.displayPopup();
      }
    }, 200);
    window.addEventListener('resize', this._displayPopup);
    window.addEventListener('scroll', this._displayPopup);
  },
  beforeDestroy: function beforeDestroy() {
    if (this.popupElm && this.popupElm.parentNode === document.body) {
      document.body.removeChild(this.popupElm);
    }
    window.removeEventListener('resize', this._displayPopup);
    window.removeEventListener('scroll', this._displayPopup);
  },

  methods: {
    initCalendar: function initCalendar() {
      this.handleValueChange(this.value);
      this.displayPopup();
    },
    stringify: function stringify(date) {
      return isPlainObject(this.format) && typeof this.format.stringify === 'function' ? this.format.stringify(date) : formatDate(date, this.format, this.lang);
    },
    parse: function parse(value) {
      return isPlainObject(this.format) && typeof this.format.parse === 'function' ? this.format.parse(value) : parseDate(value, this.format, this.lang);
    },
    isValidValue: function isValidValue(value) {
      var value2date = this.transform.value2date;

      return isValidDate(value2date(value));
    },
    isValidRangeValue: function isValidRangeValue(value) {
      var value2date = this.transform.value2date;

      return Array.isArray(value) && value.length === 2 && this.isValidValue(value[0]) && this.isValidValue(value[1]) && value2date(value[1]).getTime() >= value2date(value[0]).getTime();
    },
    dateEqual: function dateEqual(a, b) {
      return isDateObejct(a) && isDateObejct(b) && a.getTime() === b.getTime();
    },
    rangeEqual: function rangeEqual(a, b) {
      var _this2 = this;

      return Array.isArray(a) && Array.isArray(b) && a.length === b.length && a.every(function (item, index) {
        return _this2.dateEqual(item, b[index]);
      });
    },
    selectRange: function selectRange(range) {
      if (typeof range.onClick === 'function') {
        var close = range.onClick(this);
        if (close !== false) {
          this.closePopup();
        }
      } else {
        this.currentValue = [new Date(range.start), new Date(range.end)];
        this.updateDate(true);
        this.closePopup();
      }
    },
    clearDate: function clearDate() {
      var date = this.range ? [null, null] : null;
      this.currentValue = date;
      this.updateDate(true);
      this.$emit('clear');
    },
    confirmDate: function confirmDate() {
      var valid = this.range ? isValidRangeDate(this.currentValue) : isValidDate(this.currentValue);
      if (valid) {
        this.updateDate(true);
      }
      this.emitDate('confirm');
      this.closePopup();
    },
    updateDate: function updateDate() {
      var confirm = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (this.confirm && !confirm || this.disabled) {
        return false;
      }
      var equal = this.range ? this.rangeEqual(this.value, this.currentValue) : this.dateEqual(this.value, this.currentValue);
      if (equal) {
        return false;
      }
      this.emitDate('input');
      this.emitDate('change');
      return true;
    },
    emitDate: function emitDate(eventName) {
      var date2value = this.transform.date2value;

      var value = this.range ? this.currentValue.map(date2value) : date2value(this.currentValue);
      this.$emit(eventName, value);
    },
    handleValueChange: function handleValueChange(value) {
      var value2date = this.transform.value2date;

      if (this.range) {
        this.currentValue = this.isValidRangeValue(value) ? value.map(value2date) : [null, null];
      } else {
        this.currentValue = this.isValidValue(value) ? value2date(value) : null;
      }
    },
    selectDate: function selectDate(date) {
      this.currentValue = date;
      this.updateDate() && this.closePopup();
    },
    selectStartDate: function selectStartDate(date) {
      this.$set(this.currentValue, 0, date);
      if (this.currentValue[1]) {
        this.updateDate();
      }
    },
    selectEndDate: function selectEndDate(date) {
      this.$set(this.currentValue, 1, date);
      if (this.currentValue[0]) {
        this.updateDate();
      }
    },
    selectTime: function selectTime(time, close) {
      this.currentValue = time;
      this.updateDate() && close && this.closePopup();
    },
    selectStartTime: function selectStartTime(time) {
      this.selectStartDate(time);
    },
    selectEndTime: function selectEndTime(time) {
      this.selectEndDate(time);
    },
    showPopup: function showPopup() {
      if (this.disabled) {
        return;
      }
      this.popupVisible = true;
    },
    closePopup: function closePopup() {
      this.popupVisible = false;
    },
    getPopupSize: function getPopupSize(element) {
      var originalDisplay = element.style.display;
      var originalVisibility = element.style.visibility;
      element.style.display = 'block';
      element.style.visibility = 'hidden';
      var styles = window.getComputedStyle(element);
      var width = element.offsetWidth + parseInt(styles.marginLeft) + parseInt(styles.marginRight);
      var height = element.offsetHeight + parseInt(styles.marginTop) + parseInt(styles.marginBottom);
      var result = { width: width, height: height };
      element.style.display = originalDisplay;
      element.style.visibility = originalVisibility;
      return result;
    },
    displayPopup: function displayPopup() {
      var dw = document.documentElement.clientWidth;
      var dh = document.documentElement.clientHeight;
      var InputRect = this.$el.getBoundingClientRect();
      var PopupRect = this._popupRect || (this._popupRect = this.getPopupSize(this.$refs.calendar));
      var position = {};
      var offsetRelativeToInputX = 0;
      var offsetRelativeToInputY = 0;
      if (this.appendToBody) {
        offsetRelativeToInputX = window.pageXOffset + InputRect.left;
        offsetRelativeToInputY = window.pageYOffset + InputRect.top;
      }
      if (dw - InputRect.left < PopupRect.width && InputRect.right < PopupRect.width) {
        position.left = offsetRelativeToInputX - InputRect.left + 1 + 'px';
      } else if (InputRect.left + InputRect.width / 2 <= dw / 2) {
        position.left = offsetRelativeToInputX + 'px';
      } else {
        position.left = offsetRelativeToInputX + InputRect.width - PopupRect.width + 'px';
      }
      if (InputRect.top <= PopupRect.height && dh - InputRect.bottom <= PopupRect.height) {
        position.top = offsetRelativeToInputY + dh - InputRect.top - PopupRect.height + 'px';
      } else if (InputRect.top + InputRect.height / 2 <= dh / 2) {
        position.top = offsetRelativeToInputY + InputRect.height + 'px';
      } else {
        position.top = offsetRelativeToInputY - PopupRect.height + 'px';
      }
      if (position.top !== this.position.top || position.left !== this.position.left) {
        this.position = position;
      }
    },
    blur: function blur() {
      this.$refs.input.blur();
    },
    handleBlur: function handleBlur(event) {
      this.$emit('blur', event);
    },
    handleFocus: function handleFocus(event) {
      if (!this.popupVisible) {
        this.showPopup();
      }
      this.$emit('focus', event);
    },
    handleKeydown: function handleKeydown(event) {
      var keyCode = event.keyCode;
      // Tab 9 or Enter 13
      if (keyCode === 9 || keyCode === 13) {
        // ie emit the watch before the change event
        event.stopPropagation();
        this.handleChange();
        this.userInput = null;
        this.closePopup();
      }
    },
    handleInput: function handleInput(event) {
      this.userInput = event.target.value;
    },
    handleChange: function handleChange() {
      if (this.editable && this.userInput !== null) {
        var value = this.text;
        var checkDate = this.$refs.calendarPanel.isDisabledTime;
        if (!value) {
          this.clearDate();
          return;
        }
        if (this.range) {
          var range = value.split(' ' + this.rangeSeparator + ' ');
          if (range.length === 2) {
            var start = this.parse(range[0]);
            var end = this.parse(range[1]);
            if (start && end && !checkDate(start, null, end) && !checkDate(end, start, null)) {
              this.currentValue = [start, end];
              this.updateDate(true);
              this.closePopup();
              return;
            }
          }
        } else {
          var date = this.parse(value);
          if (date && !checkDate(date, null, null)) {
            this.currentValue = date;
            this.updateDate(true);
            this.closePopup();
            return;
          }
        }
        this.$emit('input-error', value);
      }
    }
  }
};

/* script */
var __vue_script__$1 = script$1;

/* template */
var __vue_render__$1 = function __vue_render__() {
  var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;return _c('div', { directives: [{ name: "clickoutside", rawName: "v-clickoutside", value: _vm.closePopup, expression: "closePopup" }], staticClass: "mx-datepicker", class: {
      'mx-datepicker-range': _vm.range,
      'disabled': _vm.disabled
    }, style: {
      'width': _vm.computedWidth
    } }, [_c('div', { staticClass: "mx-input-wrapper", on: { "click": function click($event) {
        $event.stopPropagation();return _vm.showPopup($event);
      } } }, [_c('input', _vm._b({ ref: "input", class: _vm.inputClass, attrs: { "name": _vm.inputName, "type": "text", "autocomplete": "off", "disabled": _vm.disabled, "readonly": !_vm.editable, "placeholder": _vm.innerPlaceholder }, domProps: { "value": _vm.text }, on: { "keydown": _vm.handleKeydown, "focus": _vm.handleFocus, "blur": _vm.handleBlur, "input": _vm.handleInput, "change": _vm.handleChange } }, 'input', _vm.inputAttr, false)), _vm._v(" "), _vm.showClearIcon ? _c('span', { staticClass: "mx-input-append mx-clear-wrapper", on: { "click": function click($event) {
        $event.stopPropagation();return _vm.clearDate($event);
      } } }, [_vm._t("mx-clear-icon", [_c('i', { staticClass: "mx-input-icon mx-clear-icon" })])], 2) : _vm._e(), _vm._v(" "), _c('span', { staticClass: "mx-input-append" }, [_vm._t("calendar-icon", [_c('svg', { staticClass: "mx-calendar-icon", attrs: { "xmlns": "http://www.w3.org/2000/svg", "version": "1.1", "viewBox": "0 0 200 200" } }, [_c('rect', { attrs: { "x": "13", "y": "29", "rx": "14", "ry": "14", "width": "174", "height": "158", "fill": "transparent" } }), _vm._v(" "), _c('line', { attrs: { "x1": "46", "x2": "46", "y1": "8", "y2": "50" } }), _vm._v(" "), _c('line', { attrs: { "x1": "154", "x2": "154", "y1": "8", "y2": "50" } }), _vm._v(" "), _c('line', { attrs: { "x1": "13", "x2": "187", "y1": "70", "y2": "70" } }), _vm._v(" "), _c('text', { attrs: { "x": "50%", "y": "135", "font-size": "90", "stroke-width": "1", "text-anchor": "middle", "dominant-baseline": "middle" } }, [_vm._v(_vm._s(new Date().getJalaliDate()))])])])], 2)]), _vm._v(" "), _c('div', { directives: [{ name: "show", rawName: "v-show", value: _vm.popupVisible, expression: "popupVisible" }], ref: "calendar", staticClass: "mx-datepicker-popup", style: _vm.innerPopupStyle, on: { "click": function click($event) {
        $event.stopPropagation();$event.preventDefault();
      } } }, [_vm._t("header", [_vm.range && _vm.innerShortcuts.length ? _c('div', { staticClass: "mx-shortcuts-wrapper" }, _vm._l(_vm.innerShortcuts, function (range, index) {
    return _c('button', { key: index, staticClass: "mx-shortcuts", attrs: { "type": "button" }, on: { "click": function click($event) {
          return _vm.selectRange(range);
        } } }, [_vm._v(_vm._s(range.text))]);
  }), 0) : _vm._e()]), _vm._v(" "), !_vm.range ? _c('calendar-panel', _vm._b({ ref: "calendarPanel", attrs: { "index": -1, "type": _vm.innerType, "date-format": _vm.innerDateFormat, "value": _vm.currentValue, "visible": _vm.popupVisible }, on: { "select-date": _vm.selectDate, "select-time": _vm.selectTime } }, 'calendar-panel', _vm.$attrs, false)) : _c('div', { staticClass: "mx-range-wrapper" }, [_c('calendar-panel', _vm._b({ ref: "calendarPanel", staticStyle: { "box-shadow": "1px 0 rgba(0, 0, 0, .1)" }, attrs: { "index": 0, "type": _vm.innerType, "date-format": _vm.innerDateFormat, "value": _vm.currentValue[0], "end-at": _vm.currentValue[1], "start-at": null, "visible": _vm.popupVisible }, on: { "select-date": _vm.selectStartDate, "select-time": _vm.selectStartTime } }, 'calendar-panel', _vm.$attrs, false)), _vm._v(" "), _c('calendar-panel', _vm._b({ attrs: { "index": 1, "type": _vm.innerType, "date-format": _vm.innerDateFormat, "value": _vm.currentValue[1], "start-at": _vm.currentValue[0], "end-at": null, "visible": _vm.popupVisible }, on: { "select-date": _vm.selectEndDate, "select-time": _vm.selectEndTime } }, 'calendar-panel', _vm.$attrs, false))], 1), _vm._v(" "), _vm._t("footer", [_vm.confirm ? _c('div', { staticClass: "mx-datepicker-footer" }, [_c('button', { staticClass: "mx-datepicker-btn mx-datepicker-btn-confirm", attrs: { "type": "button" }, on: { "click": _vm.confirmDate } }, [_vm._v(_vm._s(_vm.confirmText))])]) : _vm._e()], { "confirm": _vm.confirmDate })], 2)]);
};
var __vue_staticRenderFns__$1 = [];

/* style */
var __vue_inject_styles__$1 = undefined;
/* scoped */
var __vue_scope_id__$1 = undefined;
/* module identifier */
var __vue_module_identifier__$1 = undefined;
/* functional template */
var __vue_is_functional_template__$1 = false;
/* style inject */

/* style inject SSR */

var DatePicker = normalizeComponent_1({ render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 }, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, undefined, undefined);

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = "@charset \"UTF-8\";\n.mx-datepicker {\n  position: relative;\n  display: inline-block;\n  width: 210px;\n  color: #73879c;\n  font: 14px/1.5 'Helvetica Neue', Helvetica, Arial, 'Microsoft Yahei', sans-serif; }\n  .mx-datepicker * {\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box; }\n  .mx-datepicker.disabled {\n    opacity: 0.7;\n    cursor: not-allowed; }\n\n.mx-datepicker-range {\n  width: 320px; }\n\n.mx-datepicker-popup {\n  position: absolute;\n  margin-top: 1px;\n  margin-bottom: 1px;\n  border: 1px solid #d9d9d9;\n  background-color: #fff;\n  -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);\n          box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);\n  z-index: 1000; }\n\n.mx-input-wrapper {\n  position: relative; }\n  .mx-input-wrapper .mx-clear-wrapper {\n    display: none; }\n  .mx-input-wrapper:hover .mx-clear-wrapper {\n    display: block; }\n  .mx-input-wrapper:hover .mx-clear-wrapper + .mx-input-append {\n    display: none; }\n\n.mx-input {\n  display: inline-block;\n  width: 100%;\n  height: 34px;\n  padding: 6px 30px;\n  padding-right: 10px;\n  font-size: 14px;\n  line-height: 1.4;\n  color: #555;\n  background-color: #fff;\n  border: 1px solid #ccc;\n  border-radius: 4px;\n  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n  text-align: right; }\n  .mx-input:disabled, .mx-input.disabled {\n    opacity: 0.7;\n    cursor: not-allowed; }\n  .mx-input:focus {\n    outline: none; }\n  .mx-input::-ms-clear {\n    display: none; }\n\n.mx-input-append {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 30px;\n  height: 100%;\n  padding: 6px;\n  background-color: #fff;\n  background-clip: content-box; }\n\n.mx-input-icon {\n  display: inline-block;\n  width: 100%;\n  height: 100%;\n  font-style: normal;\n  color: #555;\n  text-align: center;\n  cursor: pointer; }\n\n.mx-calendar-icon {\n  width: 100%;\n  height: 100%;\n  color: #555;\n  stroke-width: 8px;\n  stroke: currentColor;\n  fill: currentColor; }\n\n.mx-clear-icon::before {\n  display: inline-block;\n  content: '\\2716';\n  vertical-align: middle; }\n\n.mx-clear-icon::after {\n  content: '';\n  display: inline-block;\n  width: 0;\n  height: 100%;\n  vertical-align: middle; }\n\n.mx-range-wrapper {\n  width: 496px;\n  overflow: hidden; }\n\n.mx-shortcuts-wrapper {\n  text-align: right;\n  padding: 0 12px;\n  line-height: 34px;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.05);\n  direction: rtl; }\n  .mx-shortcuts-wrapper .mx-shortcuts {\n    background: none;\n    outline: none;\n    border: 0;\n    color: #48576a;\n    margin: 0;\n    padding: 0;\n    white-space: nowrap;\n    cursor: pointer; }\n    .mx-shortcuts-wrapper .mx-shortcuts:hover {\n      color: #419dec; }\n    .mx-shortcuts-wrapper .mx-shortcuts:after {\n      content: '|';\n      margin: 0 10px;\n      color: #48576a; }\n    .mx-shortcuts-wrapper .mx-shortcuts:last-child::after {\n      display: none; }\n\n.mx-datepicker-footer {\n  padding: 4px;\n  clear: both;\n  text-align: right;\n  border-top: 1px solid rgba(0, 0, 0, 0.05); }\n\n.mx-datepicker-btn {\n  font-size: 12px;\n  line-height: 1;\n  padding: 7px 15px;\n  margin: 0 5px;\n  cursor: pointer;\n  background-color: transparent;\n  outline: none;\n  border: none;\n  border-radius: 3px; }\n\n.mx-datepicker-btn-confirm {\n  border: 1px solid rgba(0, 0, 0, 0.1);\n  color: #73879c; }\n  .mx-datepicker-btn-confirm:hover {\n    color: #1284e7;\n    border-color: #1284e7; }\n\n/* 日历组件 */\n.mx-calendar {\n  float: right;\n  color: #73879c;\n  padding: 6px 12px;\n  font: 14px/1.5 Helvetica Neue,Helvetica,Arial,Microsoft Yahei,sans-serif; }\n  .mx-calendar * {\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box; }\n\n.mx-calendar-header {\n  padding: 0 4px;\n  height: 34px;\n  line-height: 34px;\n  text-align: center;\n  overflow: hidden;\n  direction: rtl; }\n  .mx-calendar-header > a {\n    color: inherit;\n    text-decoration: none;\n    cursor: pointer; }\n    .mx-calendar-header > a:hover {\n      color: #419dec; }\n  .mx-icon-last-month, .mx-icon-last-year,\n  .mx-icon-next-month,\n  .mx-icon-next-year {\n    padding: 0 6px;\n    font-size: 20px;\n    line-height: 30px;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n  .mx-icon-last-month, .mx-icon-last-year {\n    float: right; }\n  \n  .mx-icon-next-month,\n  .mx-icon-next-year {\n    float: left; }\n\n.mx-calendar-content {\n  width: 224px;\n  height: 224px;\n  direction: rtl; }\n  .mx-calendar-content .cell {\n    vertical-align: middle;\n    cursor: pointer; }\n    .mx-calendar-content .cell:hover {\n      background-color: #eaf8fe; }\n    .mx-calendar-content .cell.actived {\n      color: #fff;\n      background-color: #1284e7; }\n    .mx-calendar-content .cell.inrange {\n      background-color: #eaf8fe; }\n    .mx-calendar-content .cell.disabled {\n      cursor: not-allowed;\n      color: #ccc;\n      background-color: #f3f3f3; }\n\n.mx-panel {\n  width: 100%;\n  height: 100%;\n  text-align: center; }\n\n.mx-panel-date {\n  table-layout: fixed;\n  border-collapse: collapse;\n  border-spacing: 0; }\n  .mx-panel-date td, .mx-panel-date th {\n    font-size: 12px;\n    width: 32px;\n    height: 32px;\n    padding: 0;\n    overflow: hidden;\n    text-align: center; }\n  .mx-panel-date td.today {\n    color: #2a90e9; }\n  .mx-panel-date td.last-month, .mx-panel-date td.next-month {\n    color: #ddd; }\n\n.mx-panel-year {\n  padding: 7px 0; }\n  .mx-panel-year .cell {\n    display: inline-block;\n    width: 40%;\n    margin: 1px 5%;\n    line-height: 40px; }\n\n.mx-panel-month .cell {\n  display: inline-block;\n  width: 30%;\n  line-height: 40px;\n  margin: 8px 1.5%; }\n\n.mx-time-list {\n  position: relative;\n  float: left;\n  margin: 0;\n  padding: 0;\n  list-style: none;\n  width: 100%;\n  height: 100%;\n  border-top: 1px solid rgba(0, 0, 0, 0.05);\n  border-left: 1px solid rgba(0, 0, 0, 0.05);\n  overflow-y: auto;\n  /* 滚动条滑块 */ }\n  .mx-time-list .mx-time-picker-item {\n    display: block;\n    text-align: left;\n    padding-left: 10px; }\n  .mx-time-list:first-child {\n    border-left: 0; }\n  .mx-time-list .cell {\n    width: 100%;\n    font-size: 12px;\n    height: 30px;\n    line-height: 30px; }\n  .mx-time-list::-webkit-scrollbar {\n    width: 8px;\n    height: 8px; }\n  .mx-time-list::-webkit-scrollbar-thumb {\n    background-color: rgba(0, 0, 0, 0.05);\n    border-radius: 10px;\n    -webkit-box-shadow: inset 1px 1px 0 rgba(0, 0, 0, 0.1);\n            box-shadow: inset 1px 1px 0 rgba(0, 0, 0, 0.1); }\n  .mx-time-list:hover::-webkit-scrollbar-thumb {\n    background-color: rgba(0, 0, 0, 0.2); }\n";
styleInject(css);

// install function executed by Vue.use()
function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Vue.component(DatePicker.name, DatePicker);
}

// Create module definition for Vue.use()
var plugin = {
  install: install

  // To auto-install when vue is found
  /* global window global */
};var GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// Inject install function into component - allows component
// to be registered via Vue.use() as well as Vue.component()
DatePicker.install = install;

// It's possible to expose named exports when writing components that can
// also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
// export const RollupDemoDirective = component;

export default DatePicker;
