import _mergeJSXProps from 'babel-helper-vue-jsx-merge-props';

function isDateObejct(value) {
  return value instanceof Date;
}

function isValidDate(date) {
  if (date === null || date === undefined || date === '') {
    return false;
  }
  return !isNaN(new Date(date).getTime());
}

function parseTime(time) {
  var values = (time || '').split(':');
  if (values.length >= 2) {
    var hours = parseInt(values[0], 10);
    var minutes = parseInt(values[1], 10);
    return {
      hours: hours,
      minutes: minutes
    };
  }
  return null;
}

function formatTime(time) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '24';
  var a = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'a';

  var hours = time.hours;
  hours = type === '24' ? hours : hours % 12 || 12;
  hours = hours < 10 ? '0' + hours : hours;
  var minutes = time.minutes < 10 ? '0' + time.minutes : time.minutes;
  var result = hours + ':' + minutes;
  if (type === '12') {
    var suffix = time.hours >= 12 ? 'pm' : 'am';
    if (a === 'A') {
      suffix = suffix.toUpperCase();
    }
    result = result + ' ' + suffix;
  }
  return result;
}

function formatDate(date, format, locale) {
  if (!date) {
    return '';
  }
  try {
    return Date.formatJalaliDate(date, format, locale);
  } catch (e) {
    return '';
  }
}

var Languages = {
  'en': {
    'days': ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    'months': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    'pickers': ['next 7 days', 'next 30 days', 'previous 7 days', 'previous 30 days'],
    'placeholder': {
      'date': 'Select Date',
      'dateRange': 'Select Date Range'
    }
  },
  'fa': {
    'days': ['ی', 'د', 'س', 'چ', 'پ', 'ج', 'ش'],
    'months': ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
    'pickers': ['7 روز بعد', '30 روز بعد', '7 روز قبل', '30 روز قبل'],
    'placeholder': {
      'date': 'انتخاب زمان',
      'dateRange': 'انتخاب محدوده زمانی'
    }
  }
};

var defaultLang = Languages.fa;

var locale = {
  methods: {
    t: function t(path) {
      var component = this;
      var name = component.$options.name;
      while (component && (!name || name !== 'DatePicker')) {
        component = component.$parent;
        if (component) {
          name = component.$options.name;
        }
      }
      var lang = component && component.language || defaultLang;
      var arr = path.split('.');
      var current = lang;
      var value = void 0;
      for (var i = 0, len = arr.length; i < len; i++) {
        var prop = arr[i];
        value = current[prop];
        if (i === len - 1) {
          return value;
        }
        if (!value) {
          return '';
        }
        current = value;
      }
      return '';
    }
  }
};

var emitter = {
  methods: {
    dispatch: function dispatch(componentName, eventName, params) {
      var parent = this.$parent || this.$root;
      var name = parent.$options.name;

      while (parent && (!name || name !== componentName)) {
        parent = parent.$parent;

        if (parent) {
          name = parent.$options.name;
        }
      }
      if (name && name === componentName) {
        parent = parent || this;
        parent.$emit.apply(parent, [eventName].concat(params));
      }
    }
  }
};

function scrollIntoView(container, selected) {
  if (!selected) {
    container.scrollTop = 0;
    return;
  }

  var offsetParents = [];
  var pointer = selected.offsetParent;
  while (pointer && container !== pointer && container.contains(pointer)) {
    offsetParents.push(pointer);
    pointer = pointer.offsetParent;
  }
  var top = selected.offsetTop + offsetParents.reduce(function (prev, curr) {
    return prev + curr.offsetTop;
  }, 0);
  var bottom = top + selected.offsetHeight;
  var viewRectTop = container.scrollTop;
  var viewRectBottom = viewRectTop + container.clientHeight;

  if (top < viewRectTop) {
    container.scrollTop = top;
  } else if (bottom > viewRectBottom) {
    container.scrollTop = bottom - container.clientHeight;
  }
}

var PanelDate = {
  name: 'panelDate',
  mixins: [locale],
  props: {
    value: null,
    startAt: null,
    endAt: null,
    dateFormat: {
      type: String,
      default: 'YYYY-MM-DD'
    },
    calendarMonth: {
      default: new Date().getJalaliMonth()
    },
    calendarYear: {
      default: new Date().getJalaliFullYear()
    },
    firstDayOfWeek: {
      default: 7,
      type: Number,
      validator: function validator(val) {
        return val >= 1 && val <= 7;
      }
    },
    disabledDate: {
      type: Function,
      default: function _default() {
        return false;
      }
    }
  },
  methods: {
    selectDate: function selectDate(_ref) {
      var year = _ref.year,
          month = _ref.month,
          day = _ref.day;

      var date = new Date(new Date(null, null).setJalaliFullYear(year, month, day));
      if (this.disabledDate(date)) {
        return;
      }
      this.$emit('select', date);
    },
    getDays: function getDays(firstDayOfWeek) {
      var days = this.t('days');
      var firstday = parseInt(firstDayOfWeek, 10);
      return days.concat(days).slice(firstday, firstday + 7);
    },
    getDates: function getDates(year, month, firstDayOfWeek) {
      var arr = [];
      var time = new Date(new Date(null, null).setJalaliFullYear(year, month, 1));

      time.setJalaliDate(0); // 把时间切换到上个月最后一天
      var lastMonthLength = (time.getDay() + 7 - firstDayOfWeek) % 7 + 1; // time.getDay() 0是星期天, 1是星期一 ...
      var lastMonthfirst = time.getJalaliDate() - (lastMonthLength - 1);
      for (var i = 0; i < lastMonthLength; i++) {
        arr.push({ year: year, month: month - 1, day: lastMonthfirst + i });
      }

      time.setJalaliMonth(time.getJalaliMonth() + 2, 0); // 切换到这个月最后一天
      var curMonthLength = time.getJalaliDate();
      for (var _i = 0; _i < curMonthLength; _i++) {
        arr.push({ year: year, month: month, day: 1 + _i });
      }

      time.setJalaliMonth(time.getJalaliMonth() + 1, 1); // 切换到下个月第一天
      var nextMonthLength = 42 - (lastMonthLength + curMonthLength);
      for (var _i2 = 0; _i2 < nextMonthLength; _i2++) {
        arr.push({ year: year, month: month + 1, day: 1 + _i2 });
      }

      return arr;
    },
    getCellClasses: function getCellClasses(date, _ref2) {
      var month = _ref2.month;

      var classes = [];
      var cellTime = new Date(date).setHours(0, 0, 0, 0);
      var today = new Date().setHours(0, 0, 0, 0);
      var curTime = this.value && new Date(this.value).setHours(0, 0, 0, 0);
      var startTime = this.startAt && new Date(this.startAt).setHours(0, 0, 0, 0);
      var endTime = this.endAt && new Date(this.endAt).setHours(0, 0, 0, 0);

      if (month < this.calendarMonth) {
        classes.push('last-month');
      } else if (month > this.calendarMonth) {
        classes.push('next-month');
      } else {
        classes.push('cur-month');
      }

      if (cellTime === today) {
        classes.push('today');
      }

      if (this.disabledDate(cellTime)) {
        classes.push('disabled');
      }

      if (curTime) {
        if (cellTime === curTime) {
          classes.push('actived');
        } else if (startTime && cellTime <= curTime) {
          classes.push('inrange');
        } else if (endTime && cellTime >= curTime) {
          classes.push('inrange');
        }
      }
      return classes;
    },
    getCellTitle: function getCellTitle(date) {
      return formatDate(date, this.dateFormat);
    }
  },
  render: function render(h) {
    var _this = this;

    var ths = this.getDays(this.firstDayOfWeek).map(function (day) {
      return h('th', [day]);
    });

    var dates = this.getDates(this.calendarYear, this.calendarMonth, this.firstDayOfWeek);
    var tbody = Array.apply(null, { length: 6 }).map(function (week, i) {
      var tds = dates.slice(7 * i, 7 * i + 7).map(function (dateObj) {
        var date = new Date(new Date(null, null).setJalaliFullYear(dateObj.year, dateObj.month, dateObj.day));
        var attrs = {
          class: _this.getCellClasses(date, dateObj)
        };
        return h(
          'td',
          _mergeJSXProps([{
            'class': 'cell'
          }, attrs, {
            attrs: {
              'data-year': date.year,
              'data-month': date.month,
              title: _this.getCellTitle(date)
            },
            on: {
              'click': _this.selectDate.bind(_this, dateObj)
            }
          }]),
          [dateObj.day]
        );
      });
      return h('tr', [tds]);
    });

    return h(
      'table',
      { 'class': 'mx-panel mx-panel-date' },
      [h('thead', [h('tr', [ths])]), h('tbody', [tbody])]
    );
  }
};

var PanelYear = {
  name: 'panelYear',
  props: {
    value: null,
    firstYear: Number,
    disabledYear: Function
  },
  methods: {
    isDisabled: function isDisabled(year) {
      if (typeof this.disabledYear === 'function' && this.disabledYear(year)) {
        return true;
      }
      return false;
    },
    selectYear: function selectYear(year) {
      if (this.isDisabled(year)) {
        return;
      }
      this.$emit('select', year);
    }
  },
  render: function render(h) {
    var _this = this;

    // 当前年代
    var firstYear = Math.floor(this.firstYear / 10) * 10;
    var currentYear = this.value && new Date(this.value).getJalaliFullYear();
    var years = Array.apply(null, { length: 10 }).map(function (_, i) {
      var year = firstYear + i;
      return h(
        'span',
        {
          'class': {
            'cell': true,
            'actived': currentYear === year,
            'disabled': _this.isDisabled(year)
          },
          on: {
            'click': _this.selectYear.bind(_this, year)
          }
        },
        [year]
      );
    });
    return h(
      'div',
      { 'class': 'mx-panel mx-panel-year' },
      [years]
    );
  }
};

var PanelMonth = {
  name: 'panelMonth',
  mixins: [locale],
  props: {
    value: null,
    calendarYear: {
      default: new Date().getJalaliFullYear()
    },
    disabledMonth: Function
  },
  methods: {
    isDisabled: function isDisabled(month) {
      if (typeof this.disabledMonth === 'function' && this.disabledMonth(month)) {
        return true;
      }
      return false;
    },
    selectMonth: function selectMonth(month) {
      if (this.isDisabled(month)) {
        return;
      }
      this.$emit('select', month);
    }
  },
  render: function render(h) {
    var _this = this;

    var months = this.t('months');
    var currentYear = this.value && new Date(this.value).getJalaliFullYear();
    var currentMonth = this.value && new Date(this.value).getJalaliMonth();
    months = months.map(function (v, i) {
      return h(
        'span',
        {
          'class': {
            'cell': true,
            'actived': currentYear === _this.calendarYear && currentMonth === i,
            'disabled': _this.isDisabled(i)
          },
          on: {
            'click': _this.selectMonth.bind(_this, i)
          }
        },
        [v]
      );
    });
    return h(
      'div',
      { 'class': 'mx-panel mx-panel-month' },
      [months]
    );
  }
};

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var PanelTime = {
  name: 'panelTime',
  props: {
    timePickerOptions: {
      type: [Object, Function],
      default: function _default() {
        return null;
      }
    },
    timeSelectOptions: {
      type: Object,
      default: function _default() {
        return null;
      }
    },
    minuteStep: {
      type: Number,
      default: 0,
      validator: function validator(val) {
        return val >= 0 && val <= 60;
      }
    },
    value: null,
    timeType: {
      type: Array,
      default: function _default() {
        return ['24', 'a'];
      }
    },
    disabledTime: Function
  },
  computed: {
    currentHours: function currentHours() {
      return this.value ? new Date(this.value).getHours() : 0;
    },
    currentMinutes: function currentMinutes() {
      return this.value ? new Date(this.value).getMinutes() : 0;
    },
    currentSeconds: function currentSeconds() {
      return this.value ? new Date(this.value).getSeconds() : 0;
    }
  },
  methods: {
    stringifyText: function stringifyText(value) {
      return ('00' + value).slice(String(value).length);
    },
    selectTime: function selectTime(time) {
      if (typeof this.disabledTime === 'function' && this.disabledTime(time)) {
        return;
      }
      this.$emit('select', new Date(time));
    },
    pickTime: function pickTime(time) {
      if (typeof this.disabledTime === 'function' && this.disabledTime(time)) {
        return;
      }
      this.$emit('pick', new Date(time));
    },
    getTimePickerOptions: function getTimePickerOptions() {
      var result = [];
      var options = this.timePickerOptions;
      if (!options) {
        return [];
      }
      if (typeof options === 'function') {
        return options() || [];
      }
      var start = parseTime(options.start);
      var end = parseTime(options.end);
      var step = parseTime(options.step);
      if (start && end && step) {
        var startMinutes = start.minutes + start.hours * 60;
        var endMinutes = end.minutes + end.hours * 60;
        var stepMinutes = step.minutes + step.hours * 60;
        var len = Math.floor((endMinutes - startMinutes) / stepMinutes);
        for (var i = 0; i <= len; i++) {
          var timeMinutes = startMinutes + i * stepMinutes;
          var hours = Math.floor(timeMinutes / 60);
          var minutes = timeMinutes % 60;
          var value = {
            hours: hours,
            minutes: minutes
          };
          result.push({
            value: value,
            label: formatTime.apply(undefined, [value].concat(_toConsumableArray(this.timeType)))
          });
        }
      }
      return result;
    }
  },
  render: function render(h) {
    var _this = this;

    var date = this.value ? new Date(this.value) : new Date().setHours(0, 0, 0, 0);
    var disabledTime = typeof this.disabledTime === 'function' && this.disabledTime;

    var pickers = this.getTimePickerOptions();
    if (Array.isArray(pickers) && pickers.length) {
      pickers = pickers.map(function (picker) {
        var pickHours = picker.value.hours;
        var pickMinutes = picker.value.minutes;
        var time = new Date(date).setHours(pickHours, pickMinutes, 0);
        return h(
          'li',
          {
            'class': {
              'mx-time-picker-item': true,
              'cell': true,
              'actived': pickHours === _this.currentHours && pickMinutes === _this.currentMinutes,
              'disabled': disabledTime && disabledTime(time)
            },
            on: {
              'click': _this.pickTime.bind(_this, time)
            }
          },
          [picker.label]
        );
      });
      return h(
        'div',
        { 'class': 'mx-panel mx-panel-time' },
        [h(
          'ul',
          { 'class': 'mx-time-list' },
          [pickers]
        )]
      );
    }

    var minuteStep = this.minuteStep || 1;
    var minuteLength = parseInt(60 / minuteStep);
    var hours = Array.apply(null, { length: 24 }).map(function (_, i) {
      return i;
    });
    var minutes = Array.apply(null, { length: minuteLength }).map(function (_, i) {
      return i * minuteStep;
    });
    var seconds = this.minuteStep === 0 ? Array.apply(null, { length: 60 }).map(function (_, i) {
      return i;
    }) : [];
    var columns = { hours: hours, minutes: minutes, seconds: seconds };

    if (this.timeSelectOptions && _typeof(this.timeSelectOptions) === 'object') {
      columns = _extends({}, columns, this.timeSelectOptions);
    }

    var hoursColumn = columns.hours.map(function (v) {
      var time = new Date(date).setHours(v);
      return h(
        'li',
        {
          'class': {
            'cell': true,
            'actived': v === _this.currentHours,
            'disabled': disabledTime && disabledTime(time)
          },
          on: {
            'click': _this.selectTime.bind(_this, time)
          }
        },
        [_this.stringifyText(v)]
      );
    });

    var minutesColumn = columns.minutes.map(function (v) {
      var time = new Date(date).setMinutes(v);
      return h(
        'li',
        {
          'class': {
            'cell': true,
            'actived': v === _this.currentMinutes,
            'disabled': disabledTime && disabledTime(time)
          },
          on: {
            'click': _this.selectTime.bind(_this, time)
          }
        },
        [_this.stringifyText(v)]
      );
    });

    var secondsColumn = columns.seconds.map(function (v) {
      var time = new Date(date).setSeconds(v);
      return h(
        'li',
        {
          'class': {
            'cell': true,
            'actived': v === _this.currentSeconds,
            'disabled': disabledTime && disabledTime(time)
          },
          on: {
            'click': _this.selectTime.bind(_this, time)
          }
        },
        [_this.stringifyText(v)]
      );
    });

    var times = [hoursColumn, minutesColumn, secondsColumn].filter(function (v) {
      return v.length > 0;
    });

    times = times.map(function (list) {
      return h(
        'ul',
        { 'class': 'mx-time-list', style: { width: 100 / times.length + '%' } },
        [list]
      );
    });

    return h(
      'div',
      { 'class': 'mx-panel mx-panel-time' },
      [times]
    );
  }
};

//
var script = {
  name: 'CalendarPanel',
  components: { PanelDate: PanelDate, PanelYear: PanelYear, PanelMonth: PanelMonth, PanelTime: PanelTime },
  mixins: [locale, emitter],
  props: {
    value: {
      default: null,
      validator: function validator(val) {
        return val === null || isValidDate(val);
      }
    },
    startAt: null,
    endAt: null,
    visible: {
      type: Boolean,
      default: false
    },
    type: {
      type: String,
      default: 'date' // ['date', 'datetime'] zendy added 'month', 'year', mxie added "time"
    },
    dateFormat: {
      type: String,
      default: 'YYYY-MM-DD'
    },
    index: Number,
    // below user set
    defaultValue: {
      validator: function validator(val) {
        return isValidDate(val);
      }
    },
    firstDayOfWeek: {
      default: 6,
      type: Number,
      validator: function validator(val) {
        return val >= 1 && val <= 7;
      }
    },
    notBefore: {
      default: null,
      validator: function validator(val) {
        return !val || isValidDate(val);
      }
    },
    notAfter: {
      default: null,
      validator: function validator(val) {
        return !val || isValidDate(val);
      }
    },
    disabledDays: {
      type: [Array, Function],
      default: function _default() {
        return [];
      }
    },
    minuteStep: {
      type: Number,
      default: 0,
      validator: function validator(val) {
        return val >= 0 && val <= 60;
      }
    },
    timeSelectOptions: {
      type: Object,
      default: function _default() {
        return null;
      }
    },
    timePickerOptions: {
      type: [Object, Function],
      default: function _default() {
        return null;
      }
    }
  },
  data: function data() {
    var now = this.getNow(this.value);
    var calendarYear = now.getJalaliFullYear();
    var calendarMonth = now.getJalaliMonth();
    var firstYear = Math.floor(calendarYear / 10) * 10;
    return {
      panel: 'NONE',
      dates: [],
      calendarMonth: calendarMonth,
      calendarYear: calendarYear,
      firstYear: firstYear
    };
  },

  computed: {
    now: {
      get: function get() {
        return new Date(new Date(null, null).setJalaliFullYear(this.calendarYear, this.calendarMonth, 1));
      },
      set: function set(val) {
        var now = new Date(val);
        this.calendarYear = now.getJalaliFullYear();
        this.calendarMonth = now.getJalaliMonth();
      }
    },
    timeType: function timeType() {
      var h = /h+/.test(this.$parent.format) ? '12' : '24';
      var a = /A/.test(this.$parent.format) ? 'A' : 'a';
      return [h, a];
    },
    timeHeader: function timeHeader() {
      if (this.type === 'time') {
        return this.$parent.format;
      }
      return this.value && formatDate(this.value, this.dateFormat, this.$parent.lang);
    },
    yearHeader: function yearHeader() {
      return this.firstYear + ' ~ ' + (this.firstYear + 10);
    },
    months: function months() {
      return this.t('months');
    },
    notBeforeTime: function notBeforeTime() {
      return this.getCriticalTime(this.notBefore);
    },
    notAfterTime: function notAfterTime() {
      return this.getCriticalTime(this.notAfter);
    }
  },
  watch: {
    value: {
      immediate: true,
      handler: 'updateNow'
    },
    visible: {
      immediate: true,
      handler: 'init'
    },
    panel: {
      handler: 'handelPanelChange'
    }
  },
  methods: {
    handelPanelChange: function handelPanelChange(panel, oldPanel) {
      var _this = this;

      this.dispatch('DatePicker', 'panel-change', [panel, oldPanel]);
      if (panel === 'YEAR') {
        this.firstYear = Math.floor(this.calendarYear / 10) * 10;
      } else if (panel === 'TIME') {
        this.$nextTick(function () {
          var list = _this.$el.querySelectorAll('.mx-panel-time .mx-time-list');
          for (var i = 0, len = list.length; i < len; i++) {
            var el = list[i];
            scrollIntoView(el, el.querySelector('.actived'));
          }
        });
      }
    },
    init: function init(val) {
      if (val) {
        var type = this.type;
        if (type === 'month') {
          this.showPanelMonth();
        } else if (type === 'year') {
          this.showPanelYear();
        } else if (type === 'time') {
          this.showPanelTime();
        } else {
          this.showPanelDate();
        }
      } else {
        this.showPanelNone();
        this.updateNow(this.value);
      }
    },
    getNow: function getNow(value) {
      return value ? new Date(value) : this.defaultValue && isValidDate(this.defaultValue) ? new Date(this.defaultValue) : new Date();
    },

    // 根据value更新日历
    updateNow: function updateNow(value) {
      var oldNow = this.now;
      this.now = this.getNow(value);
      if (this.visible && this.now !== oldNow) {
        this.dispatch('DatePicker', 'calendar-change', [new Date(this.now), new Date(oldNow)]);
      }
    },
    getCriticalTime: function getCriticalTime(value) {
      if (!value) {
        return null;
      }
      var date = new Date(value);
      if (this.type === 'year') {
        return new Date(date.getFullYear(), 0).getTime();
      } else if (this.type === 'month') {
        return new Date(date.getFullYear(), date.getMonth()).getTime();
      } else if (this.type === 'date') {
        return date.setHours(0, 0, 0, 0);
      }
      return date.getTime();
    },
    inBefore: function inBefore(time, startAt) {
      if (startAt === undefined) {
        startAt = this.startAt;
      }
      return this.notBeforeTime && time < this.notBeforeTime || startAt && time < this.getCriticalTime(startAt);
    },
    inAfter: function inAfter(time, endAt) {
      if (endAt === undefined) {
        endAt = this.endAt;
      }
      return this.notAfterTime && time > this.notAfterTime || endAt && time > this.getCriticalTime(endAt);
    },
    inDisabledDays: function inDisabledDays(time) {
      var _this2 = this;

      if (Array.isArray(this.disabledDays)) {
        return this.disabledDays.some(function (v) {
          return _this2.getCriticalTime(v) === time;
        });
      } else if (typeof this.disabledDays === 'function') {
        return this.disabledDays(new Date(time));
      }
      return false;
    },
    isDisabledYear: function isDisabledYear(year) {
      var time = new Date(null, null).setJalaliFullYear(year, 0, 1);
      var maxTime = new Date(null, null).setJalaliFullYear(year + 1, 0, 1) - 1;
      return this.inBefore(maxTime) || this.inAfter(time) || this.type === 'year' && this.inDisabledDays(time);
    },
    isDisabledMonth: function isDisabledMonth(month) {
      var time = new Date(null, null).setJalaliFullYear(this.calendarYear, month, 1);
      var maxTime = new Date(null, null).setJalaliFullYear(this.calendarYear, month + 1, 1) - 1;
      return this.inBefore(maxTime) || this.inAfter(time) || this.type === 'month' && this.inDisabledDays(time);
    },
    isDisabledDate: function isDisabledDate(date) {
      var time = new Date(date).getTime();
      var maxTime = new Date(date).setHours(23, 59, 59, 999);
      return this.inBefore(maxTime) || this.inAfter(time) || this.inDisabledDays(time);
    },
    isDisabledTime: function isDisabledTime(date, startAt, endAt) {
      var time = new Date(date).getTime();
      return this.inBefore(time, startAt) || this.inAfter(time, endAt) || this.inDisabledDays(time);
    },
    selectDate: function selectDate(date) {
      if (this.type === 'datetime') {
        var time = new Date(date);
        if (isDateObejct(this.value)) {
          time.setHours(this.value.getHours(), this.value.getMinutes(), this.value.getSeconds());
        }
        if (this.isDisabledTime(time)) {
          time.setHours(0, 0, 0, 0);
          if (this.notBefore && time.getTime() < new Date(this.notBefore).getTime()) {
            time = new Date(this.notBefore);
          }
          if (this.startAt && time.getTime() < new Date(this.startAt).getTime()) {
            time = new Date(this.startAt);
          }
        }
        this.selectTime(time);
        this.showPanelTime();
        return;
      }
      this.$emit('select-date', date);
    },
    selectYear: function selectYear(year) {
      this.changeCalendarYear(year);
      if (this.type.toLowerCase() === 'year') {
        return this.selectDate(new Date(this.now));
      }
      this.dispatch('DatePicker', 'select-year', [year, this.index]);
      this.showPanelMonth();
    },
    selectMonth: function selectMonth(month) {
      this.changeCalendarMonth(month);
      if (this.type.toLowerCase() === 'month') {
        return this.selectDate(new Date(this.now));
      }
      this.dispatch('DatePicker', 'select-month', [month, this.index]);
      this.showPanelDate();
    },
    selectTime: function selectTime(time) {
      this.$emit('select-time', time, false);
    },
    pickTime: function pickTime(time) {
      this.$emit('select-time', time, true);
    },
    changeCalendarYear: function changeCalendarYear(year) {
      this.updateNow(new Date(new Date(null, null).setJalaliFullYear(year, this.calendarMonth, 1)));
    },
    changeCalendarMonth: function changeCalendarMonth(month) {
      if (month === -1) {
        this.calendarYear = this.calendarYear - 1;
        month = 11;
      }
      this.updateNow(new Date(new Date(null, null).setJalaliFullYear(this.calendarYear, month, 1)));
    },
    getSibling: function getSibling() {
      var _this3 = this;

      var calendars = this.$parent.$children.filter(function (v) {
        return v.$options.name === _this3.$options.name;
      });
      var index = calendars.indexOf(this);
      var sibling = calendars[index ^ 1];
      return sibling;
    },
    handleIconMonth: function handleIconMonth(flag) {
      var month = this.calendarMonth;
      this.changeCalendarMonth(month + flag);
      this.$parent.$emit('change-calendar-month', {
        month: month,
        flag: flag,
        vm: this,
        sibling: this.getSibling()
      });
    },
    handleIconYear: function handleIconYear(flag) {
      if (this.panel === 'YEAR') {
        this.changePanelYears(flag);
      } else {
        var year = this.calendarYear;
        this.changeCalendarYear(year + flag);
        this.$parent.$emit('change-calendar-year', {
          year: year,
          flag: flag,
          vm: this,
          sibling: this.getSibling()
        });
      }
    },
    handleBtnYear: function handleBtnYear() {
      this.showPanelYear();
    },
    handleBtnMonth: function handleBtnMonth() {
      this.showPanelMonth();
    },
    handleTimeHeader: function handleTimeHeader() {
      if (this.type === 'time') {
        return;
      }
      this.showPanelDate();
    },
    changePanelYears: function changePanelYears(flag) {
      this.firstYear = this.firstYear + flag * 10;
    },
    showPanelNone: function showPanelNone() {
      this.panel = 'NONE';
    },
    showPanelTime: function showPanelTime() {
      this.panel = 'TIME';
    },
    showPanelDate: function showPanelDate() {
      this.panel = 'DATE';
    },
    showPanelYear: function showPanelYear() {
      this.panel = 'YEAR';
    },
    showPanelMonth: function showPanelMonth() {
      this.panel = 'MONTH';
    }
  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function __vue_render__() {
  var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;return _c('div', { staticClass: "mx-calendar", class: 'mx-calendar-panel-' + _vm.panel.toLowerCase() }, [_c('div', { staticClass: "mx-calendar-header" }, [_c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel !== 'TIME', expression: "panel !== 'TIME'" }], staticClass: "mx-icon-last-year", on: { "click": function click($event) {
        return _vm.handleIconYear(-1);
      } } }, [_vm._v("«")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], staticClass: "mx-icon-last-month", on: { "click": function click($event) {
        return _vm.handleIconMonth(-1);
      } } }, [_vm._v("‹")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel !== 'TIME', expression: "panel !== 'TIME'" }], staticClass: "mx-icon-next-year", on: { "click": function click($event) {
        return _vm.handleIconYear(1);
      } } }, [_vm._v("»")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], staticClass: "mx-icon-next-month", on: { "click": function click($event) {
        return _vm.handleIconMonth(1);
      } } }, [_vm._v("›")]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], staticClass: "mx-current-month", on: { "click": _vm.handleBtnMonth } }, [_vm._v(_vm._s(_vm.months[_vm.calendarMonth]))]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE' || _vm.panel === 'MONTH', expression: "panel === 'DATE' || panel === 'MONTH'" }], staticClass: "mx-current-year", on: { "click": _vm.handleBtnYear } }, [_vm._v(_vm._s(_vm.calendarYear))]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'YEAR', expression: "panel === 'YEAR'" }], staticClass: "mx-current-year" }, [_vm._v(_vm._s(_vm.yearHeader))]), _vm._v(" "), _c('a', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'TIME', expression: "panel === 'TIME'" }], staticClass: "mx-time-header", on: { "click": _vm.handleTimeHeader } }, [_vm._v(_vm._s(_vm.timeHeader))])]), _vm._v(" "), _c('div', { staticClass: "mx-calendar-content" }, [_c('panel-date', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'DATE', expression: "panel === 'DATE'" }], attrs: { "value": _vm.value, "date-format": _vm.dateFormat, "calendar-month": _vm.calendarMonth, "calendar-year": _vm.calendarYear, "start-at": _vm.startAt, "end-at": _vm.endAt, "first-day-of-week": _vm.firstDayOfWeek, "disabled-date": _vm.isDisabledDate }, on: { "select": _vm.selectDate } }), _vm._v(" "), _c('panel-year', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'YEAR', expression: "panel === 'YEAR'" }], attrs: { "value": _vm.value, "disabled-year": _vm.isDisabledYear, "first-year": _vm.firstYear }, on: { "select": _vm.selectYear } }), _vm._v(" "), _c('panel-month', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'MONTH', expression: "panel === 'MONTH'" }], attrs: { "value": _vm.value, "disabled-month": _vm.isDisabledMonth, "calendar-year": _vm.calendarYear }, on: { "select": _vm.selectMonth } }), _vm._v(" "), _c('panel-time', { directives: [{ name: "show", rawName: "v-show", value: _vm.panel === 'TIME', expression: "panel === 'TIME'" }], attrs: { "minute-step": _vm.minuteStep, "time-picker-options": _vm.timePickerOptions, "time-select-options": _vm.timeSelectOptions, "value": _vm.value, "disabled-time": _vm.isDisabledTime, "time-type": _vm.timeType }, on: { "select": _vm.selectTime, "pick": _vm.pickTime } })], 1)]);
};
var __vue_staticRenderFns__ = [];

/* style */
var __vue_inject_styles__ = undefined;
/* scoped */
var __vue_scope_id__ = undefined;
/* module identifier */
var __vue_module_identifier__ = undefined;
/* functional template */
var __vue_is_functional_template__ = false;
/* style inject */

/* style inject SSR */

var calendar = normalizeComponent_1({ render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ }, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, undefined, undefined);

export default calendar;
